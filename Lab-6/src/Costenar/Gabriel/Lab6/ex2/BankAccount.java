package Costenar.Gabriel.Lab6.ex2;

public class BankAccount {

    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount) {
        this.balance = this.balance - amount;

    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    public void deposit(double amount) {
        this.balance = this.balance + amount;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BankAccount) {
            BankAccount b = (BankAccount) obj;
            return balance == b.balance && b.owner.equals(owner);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return (int) balance + owner.hashCode();
    }

    public String toString() {
        return getOwner() + " " + getBalance();
    }
}
