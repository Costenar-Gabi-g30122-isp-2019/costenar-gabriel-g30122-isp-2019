package Costenar.Gabriel.Lab6.ex2;

import java.util.*;

public class Bank {

    List<BankAccount> accounts = new ArrayList<>();

    public void addAccount(String owner, double balance) {
        BankAccount b = new BankAccount(owner, balance);
        accounts.add(b);
    }


    public void printAccounts() {
        accounts.sort((BankAccount b1, BankAccount b2) -> (int) b1.getBalance() - (int) b2.getBalance());
        System.out.println(accounts);
    }

    public void printAccounts(double minBalance, double maxBalance) {

        for (BankAccount o : accounts) {
            if (o.getBalance() >= minBalance && o.getBalance() <= maxBalance)
                System.out.println(o.getOwner() + " " + o.getBalance());

        }
    }

    public BankAccount getAccount() {
        accounts.sort((BankAccount b1, BankAccount b2) -> b1.getOwner().compareTo(b2.getOwner()));
        System.out.println(accounts);
        return null;

    }
}