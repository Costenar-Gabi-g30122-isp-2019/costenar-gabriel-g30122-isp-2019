package Costenar.Gabriel.Lab6.ex3;

public class BankAccount implements Comparable{

    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount) {
        this.balance = this.balance - amount;

    }

    public int compareTo(Object o) {
        BankAccount p = (BankAccount) o;
        if(owner.compareTo(p.getOwner())==1) return 1;
        if(owner.compareTo(p.getOwner())==0) return 0;
        return -1;
    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    public void deposit(double amount) {
        this.balance = this.balance + amount;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BankAccount) {
            BankAccount b = (BankAccount) obj;
            return balance == b.balance && b.owner.equals(owner);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return (int) balance + owner.hashCode();
    }

    public String toString() {
        return getOwner() + " " + getBalance();
    }
}
