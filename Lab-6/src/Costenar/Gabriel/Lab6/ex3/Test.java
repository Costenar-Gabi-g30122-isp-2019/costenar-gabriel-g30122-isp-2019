package Costenar.Gabriel.Lab6.ex3;

public class Test {

    public static void main(String[] args) {
        Bank bank = new Bank();

        bank.addAccount("Harfas", 25);
        bank.addAccount("John", 10);
        bank.addAccount("Igor", 100);
        bank.addAccount("Rick", 60);
        bank.addAccount("Clyne", 50);

        System.out.println("Print Accounts by balance: ");
        bank.printAccounts();

        System.out.println("Print Accounts between max and min limits:");
        bank.printAccounts(40, 190);

        System.out.println("All accounts order by owner: ");
        bank.getAccount();

    }


}
