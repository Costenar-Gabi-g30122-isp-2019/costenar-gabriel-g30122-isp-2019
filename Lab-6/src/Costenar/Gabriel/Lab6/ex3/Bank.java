package Costenar.Gabriel.Lab6.ex3;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class Bank {

    TreeSet<BankAccount> accounts1 = new TreeSet<>();
    TreeSet<BankAccount> accounts2 = new TreeSet<>();

    public void addAccount(String owner, double balance) {
        BankAccount b = new BankAccount(owner, balance);
        accounts1.add(b);
        accounts2.add(b);
    }




    public void printAccounts() {
        //accounts.sort((BankAccount b1, BankAccount b2) -> (int) b1.getBalance() - (int) b2.getBalance());
        System.out.println(accounts1);
    }

    public void printAccounts(double minBalance, double maxBalance) {

        for (BankAccount o : accounts1) {
            if (o.getBalance() >= minBalance && o.getBalance() <= maxBalance)
                System.out.println(o.getOwner() + " " + o.getBalance());

        }
    }

    public BankAccount getAccount() {
        //accounts.sort((BankAccount b1, BankAccount b2) -> b1.getOwner().compareTo(b2.getOwner()));
        System.out.println(accounts2);
        return null;

    }
}