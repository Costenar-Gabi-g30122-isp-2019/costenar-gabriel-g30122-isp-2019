package Costenar.Gabriel.Lab6.ex4;

import java.util.HashMap;

public class Dictionary {

    HashMap<Word, Definition> dictionar = new HashMap<>();

    public void addWord(Word w, Definition d) {

        if (dictionar.containsKey(w)) {
            System.out.println("Existent word modified.");
        } else {
            System.out.println("Word added!");
        }
        dictionar.put(w, d);
    }

    public Definition getDefinition(Word w) {

        return dictionar.get(w);
    }

    public void getAllWords() {
        for (Word w : dictionar.keySet()) System.out.println(w.toString());

    }

    public void getAllDefinitions() {
        for (Definition d : dictionar.values()) System.out.println(d.toString());

    }


}