package Costenar.Gabriel.Lab6.ex4;

import java.util.Scanner;

public class Meniu {

    public static void main(String[] args) {
        Dictionary dex = new Dictionary();

        int exit=0;

        while(exit!=1){

            System.out.printf("\nMenu\n1 - Add word\n2 - Get definition\n3 - Get all words\n4 - Get all definitions\n5 - Exit\n");
            Scanner a1 = new Scanner(System.in);
            int ans = a1.nextInt();
            switch (ans) {
                case 1: {
                    System.out.printf("Enter the word: ");
                    Scanner w1 = new Scanner(System.in);
                    String word1 = w1.nextLine();
                    System.out.printf("Enter the definition: ");
                    Scanner d1 = new Scanner(System.in);
                    String definition1 = d1.nextLine();
                    dex.addWord(new Word(word1), new Definition(definition1));
                }
                break;

                case 2:
                    System.out.printf("Enter the word: ");
                    Scanner w2 = new Scanner(System.in);
                    String word2 = w2.nextLine();
                    Word w = new Word(word2);
                    System.out.println(dex.getDefinition(w));


                    break;

                case 3:
                    dex.getAllWords();
                    break;

                case 4:
                    dex.getAllDefinitions();
                    break;
                case 5:
                    exit=1;
                    break;

                default:
                    System.out.printf("Enter a valid number.\n");

            }

        }

    }
}

