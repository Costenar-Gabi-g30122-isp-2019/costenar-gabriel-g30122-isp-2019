package Costenar.Gabriel.Lab6.ex4;

public class Definition {

    String description;

    public Definition(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}
