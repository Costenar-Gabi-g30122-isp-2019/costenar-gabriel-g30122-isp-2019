package Costenar.Gabriel.Lab6.ex4;

public class Word {

    String name;

    public Word(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return name;
    }

    public boolean equals(Object obj) {
        if(!(obj instanceof Word))
            return false;
        Word x = (Word)obj;
        return name.equals(x.name);
    }

    public int hashCode() {
        return name.length()*1000;
    }

}
