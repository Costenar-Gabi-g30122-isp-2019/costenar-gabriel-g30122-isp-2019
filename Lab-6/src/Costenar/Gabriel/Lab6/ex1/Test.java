package Costenar.Gabriel.Lab6.ex1;

public class Test {

    public static void main(String[] args){
        BankAccount a1=new BankAccount();
        BankAccount a2=new BankAccount();
        BankAccount a3=new BankAccount();
        BankAccount a4=new BankAccount();

        a1.deposit(2);
        a1.setOwner("John");
        a2.deposit(3);
        a2.setOwner("John");
        a3.deposit(4);
        a3.setOwner("Liviu");
        a4.deposit(2);
        a4.setOwner("John");


        System.out.println(a1.equals(a2));
        System.out.println(a1.equals(a3));
        System.out.println(a1.equals(a4));
    }

}
