package Costenar.Gabriel.Lab6.ex1;

public class BankAccount {
    private String owner;
    private double balance=0;

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void withdraw(double amount) {
        balance-=amount;
    }

    public void deposit(double amount) {
        balance=amount;

    }
    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof BankAccount)) {
            return false;
        }
        BankAccount acc = (BankAccount) obj;
        return acc.owner.equals(owner) && acc.balance == balance;
    }

    public int hashCode() {

        return (int) balance + owner.hashCode();
    }


}
