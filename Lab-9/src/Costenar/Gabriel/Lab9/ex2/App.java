package Costenar.Gabriel.Lab9.ex2;

import Costenar.Gabriel.Lab9.ex1.ButtonAndTextField;
import Costenar.Gabriel.Lab9.ex1.ButtonAndTextField2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

public class App extends JFrame {

    int counter=0;
    JLabel nr;
    JLabel da;
    JButton btnIncrementare,btnDecrementare;


    App() {
        setTitle("Incrementare");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(600, 650);
        setVisible(true);
    }


    public void init() {
        this.setLayout(null);
        int width = 150;
        int height = 40;

        nr = new JLabel("Numarul");
        nr.setBounds(10, 50, width, height);

        da=new JLabel("da");
        da.setBounds(100, 50, width,height);


        btnIncrementare = new JButton("Incrementeaza");
        btnIncrementare.setBounds(20, 100, width, height);

        btnDecrementare = new JButton("Decrementeaza");
        btnDecrementare.setBounds(170, 100, width, height);

        btnIncrementare.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                counter++;
                da.setText(""+counter);
            }
        });
        btnDecrementare.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                counter--;
                da.setText(""+counter);
            }
        });
        add(btnIncrementare);add(da);add(nr);add(btnDecrementare);
    }

    public static void main(String[] args) {
        new App();
    }
}

