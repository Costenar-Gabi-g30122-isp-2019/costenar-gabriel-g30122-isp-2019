package Costenar.Gabriel.Lab9.ex3;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.io.*;

public class ReadFile extends JFrame {

    JButton button;
    JTextField text;
    JTextField inputText;
    JLabel label1;

    ReadFile() {
        setTitle("File reader");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500, 550);
        setVisible(true);
    }


    public void init() {
        this.setLayout(null);
        int width = 250;
        int height = 40;

        label1 = new JLabel();
        label1.setBounds(20,10,width,height);
        label1.setText("FileName");

        inputText = new JTextField();
        inputText.setBounds(20,60,width, height);

        text = new JTextField();
        text.setBounds(20,160,width, height);

        button = new JButton("ReadFile");
        button.setBounds(20, 260, width, height);
        button.setSize(100, 20);
        button.addActionListener(new A());

        add(button); add(text);add(inputText);add(label1);

    }

    public static void main(String[] args) {
        new ReadFile();
    }

    class A implements ActionListener {

        //temp filePath to local folder
        private String filePath = "C:\\Users\\Asus\\costenar-gabriel-g30122-isp-2019\\Lab-9\\src\\Costenar\\Gabriel\\Lab9\\ex3\\";

        @Override
        public void actionPerformed(ActionEvent e) {

            try {
                //Read the file name
                String fileName;
                fileName = ReadFile.this.inputText.getText();

                //concat de paths
                filePath +=fileName;


                //read the file
                BufferedReader in = new BufferedReader(
                        new FileReader(filePath));
                String s, s2 = new String();
                while ((s = in.readLine()) != null)
                    s2 += s + "\n";
                in.close();

                //output file context
                ReadFile.this.text.setText(s2);
            }
            catch (Exception ex)
            {
                ReadFile.this.text.setText("File not Found");
                System.out.println(ex.getStackTrace());
            }
        }
    }

}

