package Costenar.Gabriel.Lab9.ex4;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.plaf.basic.BasicOptionPaneUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class XO extends JFrame {
    JButton[] b;
    int ind;
    String caract;

    XO() {
        setTitle("Xand0");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500, 500);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 80;
        int height = 80;

        b = new JButton[9];
        b[0] = new JButton();
        b[0].setBounds(10, 10, width, height);
        b[0].addActionListener(new TratareButon());
        b[1] = new JButton();
        b[1].setBounds(10, 110, width, height);
        b[1].addActionListener(new TratareButon());
        b[2] = new JButton();
        b[2].setBounds(10, 210, width, height);
        b[2].addActionListener(new TratareButon());
        b[3] = new JButton();
        b[3].setBounds(110, 10, width, height);
        b[3].addActionListener(new TratareButon());
        b[4] = new JButton();
        b[4].setBounds(110, 110, width, height);
        b[4].addActionListener(new TratareButon());
        b[5] = new JButton();
        b[5].setBounds(110, 210, width, height);
        b[5].addActionListener(new TratareButon());
        b[6] = new JButton();
        b[6].setBounds(210, 10, width, height);
        b[6].addActionListener(new TratareButon());
        b[7] = new JButton();
        b[7].setBounds(210, 110, width, height);
        b[7].addActionListener(new TratareButon());
        b[8] = new JButton();
        b[8].setBounds(210, 210, width, height);
        b[8].addActionListener(new TratareButon());
        for (int i = 0; i < 9; i++) add(b[i]);
    }
    public void resetButtons()
    {
        for(int i = 0; i < 9; i++)
        {
            b[i].setText("");
        }
    }

    public static void main(String[] args) {
        new XO();
    }

    class TratareButon implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            JButton buttonClicked = (JButton)e.getSource(); //get the particular button that was clicked
            if(ind%2 == 0)
                buttonClicked.setText("X");
            else
                buttonClicked.setText("O");

            if(checkForWin() == true)
            {
                JOptionPane.showConfirmDialog(null, caract+" wins!");
                resetButtons();
            }
            ind++;

        }
        public boolean checkForWin()
        {

            if( checkEqual(0,1) && checkEqual(1,2) ) //primele 3 pe orizontala
            {caract=b[0].getText();return true;}
            else if( checkEqual(3,4) && checkEqual(4,5) )//al doilea rand de 3
            {caract=b[3].getText();return true;}
            else if ( checkEqual(6,7) && checkEqual(7,8))//al treilea rand
            {caract=b[6].getText();return true;}


            else if ( checkEqual(0,3) && checkEqual(3,6))//primele 3 pe verticala
            {caract=b[0].getText();return true;}
            else if ( checkEqual(1,4) && checkEqual(4,7))//al doilea rand de 3
            {caract=b[1].getText();return true;}
            else if ( checkEqual(2,5) && checkEqual(5,8))//al treilea rand
            {caract=b[2].getText();return true;}
            else if ( checkEqual(0,4) && checkEqual(4,8))
            {caract=b[0].getText();return true;}
            else if ( checkEqual(2,4) && checkEqual(4,6))
            {caract=b[2].getText();return true;}
            else
                return false;

        }
        public boolean checkEqual(int j, int i)
        {
            if ( b[j].getText().equals(b[i].getText()) && !b[j].getText().equals("") )
                return true;
            else
                return false;
        }

    }

}