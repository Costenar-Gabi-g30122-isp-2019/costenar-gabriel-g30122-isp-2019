package Costenar.Gabriel.Lab11.ex2;

import java.util.ArrayList;
import java.util.List;

public class ProductController {

    private Product model;
    private ShowProduct view;

    public ProductController(Product model, ShowProduct view) {
        this.model = model;
        this.view = view;
    }

    public String getProductName() {
        return model.getName();
    }

    public int getProductPrice() {
        return model.getPrice();
    }

    public int getProductQty() {
        return model.getQuantity();
    }

    public void setProductName(String name){
        model.setName(name);
    }

    public void setProductPrice(int price){
        model.setPrice(price);
    }

    public void setProductQty(int qty){
        model.setQuantity(qty);
    }

    public void updateView() {
        view.printProductDetails(model.getName(), model.getPrice(), model.getQuantity());
    }
}
