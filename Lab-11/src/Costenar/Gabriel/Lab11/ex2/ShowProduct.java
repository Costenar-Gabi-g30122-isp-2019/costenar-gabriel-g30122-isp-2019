package Costenar.Gabriel.Lab11.ex2;

public class ShowProduct{
    public void printProductDetails(String productName, int price, int qty) {
        System.out.println("Product:");
        System.out.println("- name: " + productName);
        System.out.println("- price: " + price);
        System.out.println("- quantity: " + qty);
    }
}
