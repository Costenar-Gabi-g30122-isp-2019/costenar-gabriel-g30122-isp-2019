package Costenar.Gabriel.Lab11.ex1;

import java.util.Observable;
import java.util.Random;

class TemperatureSensor extends Observable implements Runnable {

    Random r = new Random();
    private int watchedValue = 0;

    public TemperatureSensor(int value) {
        watchedValue = value;
    }

    public synchronized void setWatchedValue() {
        int val = r.nextInt(100 + 20) + 20;
        if (val != watchedValue) {
            Monitoring.setTemp(val);
            watchedValue = val;
            setChanged();
            notifyObservers(val);
        }
    }

    @Override
    public void run() {
        Thread t = Thread.currentThread();
        while (true) {
            setWatchedValue();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
