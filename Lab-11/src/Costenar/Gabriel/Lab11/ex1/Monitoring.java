package Costenar.Gabriel.Lab11.ex1;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

class Monitoring extends JFrame implements Observer {

    static JTextField temp;
    static JTextArea observerOutput;
    static JLabel currentTemp;
    private static StringBuilder text = new StringBuilder("Observer: no observed event.\n");

    public Monitoring() {
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 400);

        temp = new JTextField("Current temperature: 0");
        temp.setBounds(10, 10, 365, 20);
        add(temp);

        currentTemp = new JLabel("Observer dialog:");
        currentTemp.setBounds(10, 30, 300, 20);
        add(currentTemp);

        observerOutput = new JTextArea();
        observerOutput.setBounds(10, 50, 365, 250);
        add(observerOutput);

        setVisible(true);
    }

    public static void setTemp(int tempToSet) {
        text.append("Observer: Value changed to: "+String.valueOf(tempToSet));
        text.append("\n");
        temp.setText("Current temperature: " + tempToSet);
        observerOutput.setText(text.toString());
    }

    public static void main(String[] args) throws InterruptedException {
        TemperatureSensor sensor = new TemperatureSensor(0);
        Monitoring watcher = new Monitoring();
        sensor.addObserver(watcher);
        Thread thread = new Thread(sensor, "thread1");
        thread.start();
    }

    public void update(Observable obj, Object arg) {
        System.out.println("Update called with Arguments: " + arg);
    }
}