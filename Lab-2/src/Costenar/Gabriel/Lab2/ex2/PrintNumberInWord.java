package Costenar.Gabriel.Lab2.ex2;

import java.util.Scanner;

public class PrintNumberInWord {
    int number;


    public PrintNumberInWord(int number){
        this.number=number;
    }

     public void displayA(int number){
        if(number==1){
            System.out.println("ONE");
        }
         if(number==2){
             System.out.println("TWO");
         }
         if(number==3){
             System.out.println("THREE");
         }
         if(number==4){
             System.out.println("FOUR");
         }
         if(number==5){
             System.out.println("FIVE");
         }
         if(number==6){
             System.out.println("SIX");
         }
         if(number==7){
             System.out.println("SEVEN");
         }
         if(number==8){
             System.out.println("EIGHT");
         }
         if(number==9){
             System.out.println("NINE");
         }
         if(number>9||number<1){
             System.out.println("OTHER");
         }
         System.out.println("\n");

     }

     public void displayB(int number){

        switch(number){
            case 1:System.out.println("ONE");break;
            case 2:System.out.println("TWO");break;
            case 3:System.out.println("THREE");break;
            case 4:System.out.println("FOUR");break;
            case 5:System.out.println("FIVE");break;
            case 6:System.out.println("SIX");break;
            case 7:System.out.println("SEVEN");break;
            case 8:System.out.println("EIGHT");break;
            case 9:System.out.println("NINE");break;
            default:System.out.println("OTHER");break;
        }
     }

     public static void main(String[] args){

         Scanner in = new Scanner(System.in);
         int x = in.nextInt();
         PrintNumberInWord Number =new PrintNumberInWord(x);
         Number.displayA(Number.number);
         Number.displayB(Number.number);

     }
}
