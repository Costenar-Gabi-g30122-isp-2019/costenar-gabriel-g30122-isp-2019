package Costenar.Gabriel.Lab2.ex6;

import java.util.Scanner;

public class FactorialN {

    int n;

    public void FactorialNeRecursiv(int n){
        int i;
        long fact=1;
        for(i=2;i<=n;i++){
            fact*=i;
        }
        System.out.println("Factorialul calculat nerecursiv a lui "+n+" este egal cu "+fact);
    }

    public int calculFact(int n){
        if(n==1){
            return 1;
        }
        else
        {
            return n*calculFact(n-1);
        }
    }

    public void FactorialulRecursiv(int n){
        System.out.println("Factorialu calculat recursiv a lui "+n+" este egal cu "+calculFact(n));
    }



    public static void main(String[] args){
        Scanner in=new Scanner(System.in);
        int n;
        System.out.println("Numarul n=");
        n=in.nextInt();
        FactorialN f=new FactorialN();
        f.FactorialNeRecursiv(n);
        f.FactorialulRecursiv(n);
    }
}
