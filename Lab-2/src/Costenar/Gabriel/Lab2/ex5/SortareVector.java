package Costenar.Gabriel.Lab2.ex5;

import java.util.Scanner;

public class SortareVector {

    public void BubbleSort(int[] v){
        int ok=0;
        int i;
        int l=v.length;

        do {
            ok=0;
            for(i=0;i<l-1;i++){
                if(v[i]>v[i+1]){
                    int aux=v[i];
                    v[i]=v[i+1];
                    v[i+1]=aux;
                    ok=1;
                }
            }
        }while(ok==1);


    }

    public void afisareVector(int[] v){
        int l=v.length;
        System.out.println("Vectorul sortat arata astfel: ");
        for(int i=0;i<l;i++){
            System.out.println(v[i]+" ");
        }
    }



    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int[] v=new int[10];
        for(int i=0;i<10;i++){
            v[i]=in.nextInt();
        }
        SortareVector vector=new SortareVector();
        vector.BubbleSort(v);
        vector.afisareVector(v);
    }
}
