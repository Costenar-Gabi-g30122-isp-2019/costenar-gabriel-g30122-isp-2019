package Costenar.Gabriel.Lab2.Ex3;


import java.util.Scanner;
public class NumerePrimeAB {

    int primulNR, aldoileaNR;

    public NumerePrimeAB(int primulNR, int aldoileaNR) {
        this.primulNR = primulNR;
        this.aldoileaNR = aldoileaNR;
    }

    public void afisare(int primulNR, int aldoileaNR) {

        int i;
        int d;
        int cateNumere = 0;
        System.out.println("Numerele prime din intervalul (" + primulNR + "," + aldoileaNR + ") sunt:\n");
        for (i = primulNR+1; i < aldoileaNR; i++) {
            int ok=1;
            for (d = 2; d <= Math.sqrt(i);d++){
                if (i % d == 0) {
                    ok = 0;
                }
            }
            if (ok == 1) {
                System.out.println(i + " ");
                cateNumere++;
            }
        }
        System.out.println("\nExista " + cateNumere + " prime");
    }

    public static void  main(String[] args){
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        int y = in.nextInt();
        NumerePrimeAB nr=new NumerePrimeAB(x,y);
        nr.afisare(nr.primulNR,nr.aldoileaNR);
    }

}
