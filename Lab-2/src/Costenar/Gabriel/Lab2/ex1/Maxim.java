package Costenar.Gabriel.Lab2.ex1;

import java.util.Scanner;
public class Maxim {

    int nr1, nr2;
    public Maxim(int nr1,int nr2){
        this.nr1=nr1;
        this.nr2=nr2;
    }
    public void afisareMax(int nr1, int nr2) {
        if(nr1>nr2){
            System.out.println("Nr maxim: "+nr1);
        }
        if(nr2>nr1){
            System.out.println("Nr maxim: "+nr2);
        }
    }

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        int y = in.nextInt();
        Maxim m=new Maxim(x,y);
        m.afisareMax(m.nr1,m.nr2);

    }

}
