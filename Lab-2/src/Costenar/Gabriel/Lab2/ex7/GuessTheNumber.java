package Costenar.Gabriel.Lab2.ex7;

import java.util.Random;
import java.util.Scanner;

public class GuessTheNumber {

    int n;
    int counter=0;
    String level;

    public GuessTheNumber(){
        Random r=new Random();
        this.n=r.nextInt(50);
        //this.n = 2;

    }

    public int verify(int number){
        if(number==this.n){
            System.out.println("You win");
            return 0;
        }
        if(number>this.n){
            System.out.println("Wrong answer, your number is too high");
            counter++;
        }
        if(number<this.n){
            System.out.println("Wrong answer, your number is too low");
            counter++;
        }
        if(counter==3){
            System.out.println("You lose");
            return 2;
        }
        return 1;
    }


    public static void main(String[] args){
        Scanner in=new Scanner(System.in);
        GuessTheNumber N=new GuessTheNumber();

        System.out.println("Guess the number.... Your number is:");
        int x=in.nextInt();
        int ok=N.verify(x);
        while(ok!=0 && ok!=2)
        {
            System.out.println("Guess the number.... Your number is:");
            x=in.nextInt();
            ok=N.verify(x);
        }
    }
}
