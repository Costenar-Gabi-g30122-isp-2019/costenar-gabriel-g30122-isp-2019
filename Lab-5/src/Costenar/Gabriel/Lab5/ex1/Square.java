package Costenar.Gabriel.Lab5.ex1;

public class Square extends Rectangle{

    private double side;
    //metode

    public Square() {
    }

    public Square(double side) {
        this.side = side;
    }

    public Square(double side ,String color, boolean filled) {
        super(side,side, color, filled);
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    public void setWidth(){
        super.width=side;
    }

    public void setLength(){
        super.length=side;
    }

    public String toString(){
        if(super.filled==true){
            return super.color+" square with side="+side+" is filled";
        }
        else{
            return super.color+" square with side="+side+" is not filled";
        }
    }
}
