package Costenar.Gabriel.Lab5.ex1;

public class Circle extends Shape {
    protected double radius;


    //metode

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(double radius,String color, boolean filled) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    double getArea(){
        return Math.PI*radius*radius;
    }

    double getPerimeter(){
        return 2*Math.PI*radius;
    }

    public String toString(){
        if(super.filled==true){
            return super.color+" circle with radius="+radius+" is filled";
        }
        else{
            return super.color+" circle with radius="+radius+" is not filled";
        }
    }
}
