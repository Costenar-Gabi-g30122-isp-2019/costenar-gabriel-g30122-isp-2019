package Costenar.Gabriel.Lab5.ex1;

public class Rectangle extends Shape {
    protected double width;
    protected double length;

    //metode

    public Rectangle() {
    }

    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }

    public Rectangle(double width, double length,String color, boolean filled) {
        super(color, filled);
        this.width = width;
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    double getArea(){
        return length*width;
    }

    double getPerimeter(){
        return 2*(width+length);
    }

    public String toString(){
        if(super.filled==true){
            return super.color+" rectangele with length="+length+" and width="+width+" is filled";
        }
        else{
            return super.color+" rectangele with length="+length+" and width="+width+" is not filled";
        }
    }


}
