package Costenar.Gabriel.Lab5.ex1;

public class Test {

    public static void main(String[] args){
        Circle cerc=new Circle(2,"Red",true);
        System.out.println("Cerc de raza "+cerc.getRadius()+" perimetru "+cerc.getPerimeter()+" arie "+cerc.getArea());
        System.out.println(cerc.toString());


        Rectangle drept=new Rectangle(2,4,"Blue",false);
        System.out.println("Dreptunghi cu lungimea "+drept.getLength()+"si latime "+drept.getWidth()+" ,perimetru "+drept.getPerimeter()+" arie "+drept.getArea());
        System.out.println(drept.toString());

        Square patrat=new Square(2,"Yellow",true);
        System.out.println("Dreptunghi cu latura "+patrat.getSide()+" aria "+patrat.getArea()+" perimetrul "+patrat.getPerimeter());
        System.out.println(patrat.toString());


    }


}
