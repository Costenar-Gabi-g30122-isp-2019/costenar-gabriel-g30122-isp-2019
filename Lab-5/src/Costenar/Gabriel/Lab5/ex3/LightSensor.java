package Costenar.Gabriel.Lab5.ex3;

import java.util.Random;

public class LightSensor extends Sensor{

    public int readValue(){
        Random r=new Random();
        int lightSensor = r.nextInt(100);
        return lightSensor;
    }
}
