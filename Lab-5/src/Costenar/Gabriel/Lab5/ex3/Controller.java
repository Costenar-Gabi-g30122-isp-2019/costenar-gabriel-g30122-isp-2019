package Costenar.Gabriel.Lab5.ex3;

import java.util.Timer;

public class Controller {

    int i=0;
    public void control(){
        TemperatureSensor TS=new TemperatureSensor();
        LightSensor LS=new LightSensor();
        while(i<20){
            try{
                System.out.println(i+1+". Temperature: "+TS.readValue()+" ; Light: "+LS.readValue());
                i++;
                Thread.sleep(1000);
            }
            catch(Exception e){}
        }
    }
}
