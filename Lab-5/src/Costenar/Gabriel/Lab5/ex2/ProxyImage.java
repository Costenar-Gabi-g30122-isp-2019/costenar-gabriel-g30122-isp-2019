package Costenar.Gabriel.Lab5.ex2;

public class ProxyImage implements  Image {

    private Image image;
    private String fileName;
    public int proxy;

    public ProxyImage(String fileName){
        this.fileName=fileName;
    }

    public void display(){

        if(fileName=="Real"){
            image=new RealImage(fileName);
        }else if(fileName="Rotated"){
            image=new RotatedImage(fileName);
        }
        image.display();

    }
}
