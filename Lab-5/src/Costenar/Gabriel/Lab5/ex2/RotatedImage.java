package Costenar.Gabriel.Lab5.ex2;

public class RotatedImage {

    private String fileName;

    public RotatedImage(String fileName) {
        this.fileName = fileName;
        loadFromDisk(fileName);
    }

    public void display(){
        System.out.println("Display rotated "+fileName);
    }

    private void loadFromDisk(String fileName){
        System.out.println("Loading "+fileName);
    }
}
