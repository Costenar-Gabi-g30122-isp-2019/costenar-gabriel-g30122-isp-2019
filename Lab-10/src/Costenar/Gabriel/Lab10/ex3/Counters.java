package Costenar.Gabriel.Lab10.ex3;

public class Counters extends Thread {

    int counter;
    int n; //valoarea pana la cat se numara
    int count = 0;
    Thread t;

    public Counters(int n,int counter, int a,Thread thread){
        this.n = n;
        this.counter = counter;
        this.count = a;//valoarea de la care se incepe numararea
        this.t=thread;
    }

    public static void main(String[] args) {
        Counters c1 = new Counters(100,1,0,null);
        Counters c2 = new Counters(200,2,100,c1);
        c1.start();
        c2.start();
    }

    public void run() {

        try{
            if(t!=null) t.join();
            while(count< n){
                count++;
                System.out.println("Counter"+counter+":"+count);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}