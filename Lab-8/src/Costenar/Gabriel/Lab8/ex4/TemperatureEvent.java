package Costenar.Gabriel.Lab8.ex4;

public class TemperatureEvent extends  Event {
    private int value;

    TemperatureEvent(int value) {
        super(EventType.TEMPERATURE);
        this.value = value;
    }

    int getVlaue() {
        return value;
    }

    @Override
    public String toString() {
        return "TemperatureEvent{" + "vlaue=" + value + '}';
    }

}
