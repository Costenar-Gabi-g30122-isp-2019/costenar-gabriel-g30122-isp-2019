package Costenar.Gabriel.Lab4.ex6;

public class Test {

    public static void main(String[] args) {
        Shape shape = new Shape("red", false);
        System.out.println(shape.toString());

        Circle cerc1 = new Circle(5, "blue", true);
        Circle cerc2 = new Circle(4, "black", false);
        System.out.println("Aria cerc1: " + cerc1.getArea() + " perimetrul: " + cerc1.getPerimeter());
        System.out.println("Aria cerc2: " + cerc2.getArea() + " perimetrul: " + cerc2.getPerimeter());
        System.out.println(cerc1.toString());
        System.out.println(cerc2.toString());

        Rectangle rect1 = new Rectangle(2.0, 3.0, "green", true);
        Rectangle rect2 = new Rectangle(4.0,5.0);
        System.out.println("Aria rect1 "+rect2.getArea());
        System.out.println(rect1.toString());

        Square sq1 = new Square(6,"yellow",false);
        sq1.setLength(sq1.getSide());
        sq1.setWidth(sq1.getSide());
        System.out.println(sq1.getArea());
        System.out.println(sq1.toString());
        System.out.println(sq1.getArea()+" "+sq1.getPerimeter());
    }
}

