package Costenar.Gabriel.Lab4.ex6;


public class Square extends Rectangle {

    private double side;

    public Square() {
    }

    public Square(double side) {
        this.side = side;
    }

    public Square(double side, String color, boolean filled) {
        super(side, side, color, filled);
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    public void setWidth(double side) {
        super.setWidth(this.getSide());
    }

    public void setLength(double side) {
        super.setLength(this.getSide());
    }

    public String toString() {
        return "A Square with side=" + this.side + ", which is a subclass of " + super.toString();
    }
}

