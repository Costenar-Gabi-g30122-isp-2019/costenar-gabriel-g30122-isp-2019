package Costenar.Gabriel.Lab4.ex1;

public class Circle {
    double radius=1.0;
    String color="red";

    public  Circle(){}

    public  Circle(double radius){
        this.radius=radius;
    }

    public double getRadius(){
        return this.radius;
    }

    public double getArea(){
        return this.radius*this.radius*Math.PI;
    }



}
