package Costenar.Gabriel.Lab4.ex5;

public class TestCylinder {

    public static void main(String[] args){
        Cylinder cil1=new Cylinder(2);
        Cylinder cil2=new Cylinder(1,2);
        System.out.println("Height="+cil2.getHeight());
        System.out.println("Volume="+cil2.getVolume());
    }
}
