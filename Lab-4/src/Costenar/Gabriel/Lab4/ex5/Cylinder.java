package Costenar.Gabriel.Lab4.ex5;

public class Cylinder extends Circle {

    double height=1.0;

    public Cylinder() {
    }

    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume(){
        return super.getArea()*height;
    }
}
