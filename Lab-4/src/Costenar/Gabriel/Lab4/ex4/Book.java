package Costenar.Gabriel.Lab4.ex4;


import Costenar.Gabriel.Lab4.ex2.Author;

public class Book {

    String name;
    Author[] author=new Author[3];
    double price;
    int qtyInStock=0;

    public Book(String name, Author[] author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }

    public Book(String name, Author[] author, double price, int qtyInStock) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public String toString(){
        return this.name+" by "+this.author.length+" authors";
    }

    public void printAuthors(){
        int i;
        for(i=0;i<author.length;i++){
            System.out.println(author[i].getName());
        }
    }


}
