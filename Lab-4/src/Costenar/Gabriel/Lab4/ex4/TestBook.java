package Costenar.Gabriel.Lab4.ex4;

import Costenar.Gabriel.Lab4.ex2.Author;

public class TestBook {
    public static void main(String[] args){

        Author[] a=new Author[2];
        a[0]=new Author("NumeAutor","email",'f');
        a[1]=new Author("Autor2","email2",'m');
        Book b=new Book("NumeCarte",a,2);
        System.out.println(b.toString());
        b.printAuthors();
    }
}
