package Costenar.Gabriel.Lab7.ex1;

    public class Exception1 extends Exception {
        int i;

        public Exception1(String msg, int i) {
            super(msg);
            this.i=i;
        }
        public int getI(){
            return i;
        }
    }

