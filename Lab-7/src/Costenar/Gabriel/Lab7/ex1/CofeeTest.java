package Costenar.Gabriel.Lab7.ex1;

public class CofeeTest {

    public static void main(String[] args) {
        CofeeMaker mk = new CofeeMaker();
        CofeeDrinker d = new CofeeDrinker();
        Cofee c;
        for(int i = 0;i<15;i++){
            try{c = mk.makeCofee();
            } catch(Exception1 e){
                System.out.println(e.getMessage());
            }

            try {
                d.drinkCofee(c);
            } catch (TemperatureException e) {
                System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception:"+e.getMessage()+" conc="+e.getConc());
            }
            finally{
                System.out.println("Throw the cofee cup.\n");
            }
        }
    }
}
