package Costenar.Gabriel.Lab3.ex1;

public class Robot {

    int x; //position of robot

    public Robot(){
        this.x=-1;
    }

    public void change(int k){
        if(k>=1){
            this.x=k;
        }
    }

    public String toString(){
        return "Position is "+this.x;
    }



}
