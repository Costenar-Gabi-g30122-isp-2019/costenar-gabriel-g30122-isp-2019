package Costenar.Gabriel.Lab3.ex4;

import java.util.Scanner;

public class TestMyPoint {

    public static void main(String[] args){
        int x,y;
        Scanner s=new Scanner(System.in);
        System.out.println("Introdu coordonatele punctului");
        x=s.nextInt();
        y=s.nextInt();
        MyPoint p0=new MyPoint();
        System.out.println("Default:("+p0.getX()+","+p0.getY()+")");

        MyPoint p1=new MyPoint(x,y);

        MyPoint p2=new MyPoint();
        p2.setX(1);
        p2.setY(1);

        MyPoint p3=new MyPoint();
        p3.setXY(1,2);

        System.out.println("Distanta intre punctul ("+p2.getX()+","+p2.getY()+") si punctul (2,2) este "+p2.distance(2,2));
        System.out.println("Distanta intre punctul ("+p2.getX()+","+p2.getY()+") si punctul (" +p3.getX()+","+p3.getY()+") este "+p2.distance(p3));

    }
}
