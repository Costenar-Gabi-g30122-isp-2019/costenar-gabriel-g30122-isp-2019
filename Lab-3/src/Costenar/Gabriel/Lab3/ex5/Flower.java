package Costenar.Gabriel.Lab3.ex5;



public class Flower{
    int petal;
    static int count=0;
    public Flower(int p){
        petal=p;
        count++;

    }
    public int getCount(){
        return count;
    }

    public static void main(String[] args) {
        Flower f1 = new Flower(4);
        Flower f2 = new Flower(6);
        Flower f5=new Flower(1);
        System.out.println("Numarul de constructori este "+f2.getCount());
    }
}